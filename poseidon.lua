#!/usr/bin/env lua

local d = require 'pl.pretty'.dump
local lapp = require 'pl.lapp'
local ids = require 'ids'

local args = lapp [[
Performs intrusion detection on KDD Cup datasets
  -c                      Calculates labeled totals, overrides other values
  -L                      Use labeled data, defaults to unlabeled                     
  -l,--limit (default 0)  Limit lines read from file
  <file> (string)           Input file to use
]]

ids:init()

if(args.c) then
  d(ids:calculate_labeled_totals(args.limit))
  os.exit(0)
end

local stats = ids:run(args.L, args.limit, args.file)

d(stats)
