#!/usr/bin/env lua

local utils = require 'pl.utils'
local path = require 'pl.path'
local iostr = require 'pl.stringio'
local stringx = require 'pl.stringx'
local error, assert, pairs, ipairs = error, assert, pairs, ipairs
local table, io, string = table, io, string

local kdd = {
  names = 'etc/kddcup.names',
  data = {
    labeled = 'etc/kddcup.labeled',
    nolabel = 'etc/kddcup.unlabeled'
  }
}

local ids = {
  initialized = false,
  blocked = 0,
  allowed = 0,
  false_allows = 0,
  flase_blocks = 0,
  attack_types = {},
  packet_structure = {}
}

function ids:reset()
  assert(self.initialized, "must load_rules() first!")
  self.blocked = 0
  self.allowed = 0
  self.false_allows = 0
  self.false_blocks = 0
  for k,v in pairs(self.attack_types) do
    v = 0
  end
end

function ids:run(label, lines, f)
  assert(self.initialized, "must load_rules() first!")
  assert(type(label) == 'boolean')
  if lines then assert(type(lines) == 'number') end

  self:reset()
  
  local infile = label and kdd.data.labeled or kdd.data.nolabel
  if f then
    infile = f
  end

  local limit = lines and lines > 0
  local f = assert(io.open(infile))
  for line in f:lines() do
    self:handle(line)
    if limit then
      lines = lines - 1
      if lines <= 0 then break end
    end
  end
  f:close()
  local stats = {
    blocked = self.blocked,
    allowed = self.allowed
  }
  if label then 
    stats.false_allows = self.false_allows 
    stats.false_blocks = self.false_blocks
  end
  return stats
end

function ids:load_rules()
  if not path.isfile(kdd.names) then 
    error('names file not found: '..kdd.names)
  end

  -- Open the names file and read in its contents
  local f = assert(io.open(kdd.names, 'r'))
  local text = iostr.open(f:read('*all'))
  f:close()

  -- Parse the attack_types header and ordered packet structure
  local first_line = true
  local attack_types, packet_structure = {}, {}
  for line in text:lines() do
    if first_line then 
      -- Capture the delimited string values and add to attack_types table
      string.gsub(line, "([^%.,]+)", function(name) attack_types[name] = 0 end)
      first_line = false
    end
    -- Capture the packet structure identifier keys
    local key,range = string.match(line, '^([^%.,]+): (%a+)%.$')
    table.insert(packet_structure, key)
  end

  self.attack_types = attack_types
  self.packet_structure = packet_structure
end

function ids:parse_packet(packet)
  assert(self.initialized, "must load_rules() first!")
  local max_match = #self.packet_structure + 1
  local pkt = packet:sub(1, #packet-1) -- Remove trailing dot

  -- Map values to keys
  local t = stringx.split(pkt, ',', max_match)
  local p = {}
  for i,name in ipairs(self.packet_structure) do
    p[name] = t[i]
  end
  return p
end

function ids:calculate_labeled_totals(limit)
  self:reset()
  self:run(true, limit)
  return self.attack_types
end

function ids:handle(packet)
  assert(self.initialized, "must load_rules() first!")
  local pkt = self:parse_packet(packet)
  if pkt.flag == "S0" or pkt.src_bytes == "1032" then
    if pkt.attack_type then 
      self.attack_types[pkt.attack_type] = self.attack_types[pkt.attack_type] + 1
      if pkt.attack_type == 'normal' then
        self.false_blocks = self.false_blocks + 1
      end
    end
    self.blocked = self.blocked + 1
  else
    self.allowed = self.allowed + 1
    if pkt.attack_type then
      self.attack_types[pkt.attack_type] = self.attack_types[pkt.attack_type] + 1
      if pkt.attack_type ~= 'normal' then
        self.false_allows = self.false_allows + 1
      end
    end
  end
end

function ids:init()
  self:load_rules()
  self.initialized = true
end

return ids

